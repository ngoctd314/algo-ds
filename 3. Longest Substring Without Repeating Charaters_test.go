package main

import (
	"testing"
)

func Test_solve3(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "",
			args: args{
				s: " ",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve3(tt.args.s); got != tt.want {
				t.Errorf("solve3() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_solve3n(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "",
			args: args{
				s: "ab",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve3n(tt.args.s); got != tt.want {
				t.Errorf("solve3n() = %v, want %v", got, tt.want)
			}
		})
	}
}
