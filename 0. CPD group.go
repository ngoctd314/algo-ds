package main

import "fmt"

// dspID*  -  *zoneID
// map[dspID]zoneID

// map[dspID]zoneID
func genDspGroup(dspIDZoneIDList map[int][]int, dspIDList []int) []map[int][]int {
	rs := []map[int][]int{}
	lDspList := len(dspIDList)
	for i := 0; i < lDspList; i++ {
		setZone := make(map[int]struct{}, 0)
		setZone = addZoneToSet(setZone, dspIDZoneIDList[dspIDList[i]])
		for j := i + 1; j < lDspList; j++ {
			setZone = addZoneToSet(setZone, dspIDZoneIDList[dspIDList[j]])
		}
		tmp := map[int][]int{}
		for k := range setZone {
			tmp[dspIDList[i]] = append(tmp[dspIDList[i]], k)
		}
		rs = append(rs, tmp)

	}
	fmt.Println(rs)

	return rs
}

func addZoneToSet(setZone map[int]struct{}, zoneIDList []int) map[int]struct{} {
	tmpSetZone := setZone
	flag := true

	for _, v := range zoneIDList {
		if _, ok := setZone[v]; ok {
			flag = false
			break
		}
		tmpSetZone[v] = struct{}{}
	}
	if flag {
		return tmpSetZone
	}

	return setZone
}

// type dspGroup struct {
// 	group []dsp
// }

// type cashier interface {
// 	price() float64
// }

// func (d dspGroup) price() float64 {
// 	m := d.group[0].price
// 	for _, v := range d.group {
// 		if v.price > m {
// 			m = v.price
// 		}
// 	}
// 	return m
// }
