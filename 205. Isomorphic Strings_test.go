package main

import "testing"

func Test_isIsomorphic(t *testing.T) {
	type args struct {
		s string
		t string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				s: "add",
				t: "egg",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				s: "bbbaaaba",
				t: "aaabbbba",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				s: "aa",
				t: "ab",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				s: "abbaa",
				t: "cddcd",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isIsomorphic(tt.args.s, tt.args.t); got != tt.want {
				t.Errorf("isIsomorphic() = %v, want %v", got, tt.want)
			}
		})
	}
}
