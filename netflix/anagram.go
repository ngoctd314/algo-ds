package netflix

import (
	"strconv"
)

var (
	anagramSets map[string][]string
)

func init() {
	anagramSets = make(map[string][]string)
}

// anagram entry
// Complexity:
// n: len(titles): size of the list of strings
// k: max len(titles[i]): maximum length that a single string can have
// Time: O(n x k): We are counting each letter for each string in a list
// Space: O(n x k): Since every string is being stored as a value in the dictionary whose size can be n, and the size of the string can be k, so space complexity is O(n x k)
func anagram(titles []string, query string) []string {
	initAnagramSets(titles)

	rs := getAngramGroup(query)

	return rs

}

func getAngramGroup(str string) []string {
	key := hashAnagram(str)

	rs, ok := anagramSets[key]
	if ok {
		return rs
	}

	return nil
}

func initAnagramSets(strs []string) {
	if len(strs) == 0 {
		return
	}

	// add anagram to set with key = hash
	for _, str := range strs {
		key := hashAnagram(str)
		anagramSets[key] = append(anagramSets[key], str)
	}

	return
}

// hashAnagram get key of an anagram
func hashAnagram(str string) string {
	// hash anagram
	counts := make([]int, 26)
	for _, character := range str {
		// character - 'a' => 0,1,2... (a,b,c)
		counts[character-'a']++
	}
	key := ""
	for _, v := range counts {
		key += strconv.Itoa(v)
	}

	return string(key)
}
