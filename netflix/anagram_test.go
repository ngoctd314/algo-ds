package netflix

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_getAngramGroup(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "",
			args: args{
				str: "abc",
			},
			want: []string{"abc"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getAngramGroup(tt.args.str); reflect.DeepEqual(got, tt.want) {
				t.Errorf("getAngramGroup() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_initAnagramSets(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			args: args{
				strs: []string{"abc", "bac"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			initAnagramSets(tt.args.strs)

			require.NotEmpty(t, anagramSets)
			require.Equal(t, anagramSets["11100000000000000000000000"][0], "abc")
			require.Equal(t, anagramSets["11100000000000000000000000"][1], "bac")
		})
	}

	t.Run("test init zero anagram set", func(t *testing.T) {
		anagramSets = make(map[string][]string)
		initAnagramSets([]string{})
		require.Empty(t, anagramSets)
	})
}

func Test_hashAnagram(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				str: "abcc",
			},
			want: "11200000000000000000000000",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := hashAnagram(tt.args.str)

			require.Equal(t, len(got), 26)
			require.Equal(t, got, tt.want)
		})
	}
}

func Test_anagram(t *testing.T) {
	anagramSets = make(map[string][]string)
	titles := []string{"abc", "acb", "cab", "abc", "aac"}
	want := []string{"abc", "acb", "cab", "abc"}
	query := "abc"

	find := anagram(titles, query)

	require.Equal(t, want, find)
}
