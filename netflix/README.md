# Project Description For Netflix

## Introduction

Netflix is the biggest video streaming platform in the world, offering movies, seasons, biographies, reality shows and more. So the engineering team at Netflix keeps trying to find better ways to display content in their consumers.

Let's pretend you're a developer on the Netflix engineering team. You are working on improving the user experience in finding content to watch. This involves the improvement of the search as well as recommendation functionality.

## Anagram

### Description

Implementing the "Group Similar Titles" feature for our "Netflix" project

Suppose the content library contains the following titles: "duel", "duel", "speed", "spede", "deul", "cars". How would you efficiently implement a functionality so that if a user misspell speed as spede, they are shown the correct title?

We want to split the list of titles into sets of words so that all words in a set are anagrams. In the above list, there are three sets: {"duel", "dule", "deul"}, {"speed", "spede"} and {"cars"}. Search results should comprise all members of the set that the search string is found in. We should pre-compute these sets instead of forming them when the user searches a title.

Example:
["duel", "dule", "speed", "spede", "deul", "cars"]

[["duel", "dule", "deul"], ["speed", "spede"], ["cars"]]

### Solution

From the above description, we see that all members of each set are chracterized by the same frequency of each alphabet. This means that the frequency of each alphabet in words belonging to the same group is equal.

Let's see how we might implement this functionality:

1. For each title, compute a 26-element vertor. Each element in this vector represents the frequency of an English letter in the corresponding title. Example: abbcc => 123.

2. Use this vector as a key to insert the titles into a Hash Map. All anagrams will be mapped to the same entry in this Hash Map. When a user searchs a word, compute the 26-element English letter frequency vecter based on the word. Search in the Hash Map using this vecter and return all the map entries.

3. Store the vector of the calculated character counts in the same Hash Map as a key and assign the respective set of anagrams as its value.

4. Return the values of the Hash Map, since each value will be an individual set.

## Fetch Top Movies

### Description

The top movies from multiple countries will combine into a single list of top rated movies. Search results for each country are produced in separate lists. Each member of a given list is ranked by popularity with 1 being most popular and decs as the rank number increases.

We will be given n lists that are all sorted in ascending order of populartiy rank. We have to combine these list into a single list be sorted by rank in ascending order, meaning from best to worst.

```txt
Keep in mind that the ranks are unique to individual movies and a single rank can be in multiple lists.
```

![fetch top movie](../assets/fetchtopmovie.png)
