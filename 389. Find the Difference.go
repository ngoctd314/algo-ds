package main

func findTheDifference(s string, t string) byte {
	st1 := make(map[rune]int)
	st2 := make(map[rune]int)

	for _, v := range s {
		st1[v]++
	}

	for _, v := range t {
		st2[v]++
	}

	var rs byte

	for _, v := range t {
		if st1[v] != st2[v] {
			rs = byte(v)
			break
		}
	}

	return rs
}
