package main

func solve217(nums []int) bool {
	type Set map[interface{}]struct{}
	s := make(Set)
	for _, num := range nums {
		if _, ok := s[num]; ok {
			return true
		}
		s[num] = struct{}{}
	}
	return false
}
