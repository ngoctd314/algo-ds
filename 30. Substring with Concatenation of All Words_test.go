package main

import (
	"reflect"
	"testing"
)

func Test_solve30(t *testing.T) {
	type args struct {
		s     string
		words []string
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				s:     "wordgoodgoodgoodbestword",
				words: []string{"word", "good", "best", "good"},
			},
			want: []int{},
		},
		{
			name: "",
			args: args{
				s:     "abababab",
				words: []string{"ab", "ba"},
			},
			want: []int{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve30(tt.args.s, tt.args.words); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solve30() = %v, want %v", got, tt.want)
			}
		})
	}
}

// "wordgoodgoodgoodbestword"
// [[]]
// ""
// []

func Test_genKey(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				s: "abc",
			},
			want: "#1#1#1#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := genKey(tt.args.s); got != tt.want {
				t.Errorf("genKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_anagram(t *testing.T) {
	type args struct {
		s     string
		words []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				s:     "barfoo",
				words: []string{"foo", "bar"},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := anagram(tt.args.s, tt.args.words); got != tt.want {
				t.Errorf("anagram() = %v, want %v", got, tt.want)
			}
		})
	}
}
