package main

import (
	"reflect"
	"testing"
)

func Test_genCPDGroupV1(t *testing.T) {
	type args struct {
		mapDspZoneList map[int][]int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				mapDspZoneList: map[int][]int{
					1001: {},
				},
			},
			want: []int{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := genCPDGroupV1(tt.args.mapDspZoneList); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("genCPDGroupV1() = %v, want %v", got, tt.want)
			}
		})
	}
}
