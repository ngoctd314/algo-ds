package main

import (
	"reflect"
	"testing"
)

// func Test_genKey(t *testing.T) {
// 	type args struct {
// 		str string
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want string
// 	}{
// 		{
// 			name: "",
// 			args: args{
// 				str: "abc",
// 			},
// 			want: "11100000000000000000000000",
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := genKey(tt.args.str); got != tt.want {
// 				t.Errorf("genKey() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func Test_solve49(t *testing.T) {
	type args struct {
		in []string
	}
	tests := []struct {
		name string
		args args
		want [][]string
	}{
		{
			name: "",
			args: args{
				in: []string{"bdddddddddd", "bbbbbbbbbbc"},
			},
			want: [][]string{{"bat"}, {"nat", "tan"}, {"ate", "eat", "tea"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve49(tt.args.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solve49() = %v, want %v", got, tt.want)
			}
		})
	}
}
