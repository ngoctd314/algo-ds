package main

func solve387(s string) int {
	m := [26]int{}
	for _, v := range s {
		m[v-'a']++
	}
	for k, v := range s {
		if m[v-'a'] == 1 {
			return k
		}
	}

	return -1
}
