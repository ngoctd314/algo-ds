package main

import (
	"math"
)

func solve219(nums []int, k int) bool {
	s := make(map[int]int)

	for j, v := range nums {
		if i, ok := s[v]; ok {
			if int(math.Abs(float64(i)-float64(j))) <= k {
				return true
			}
		}
		s[v] = j
	}

	return false
}
