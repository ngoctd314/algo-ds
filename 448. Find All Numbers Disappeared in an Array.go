package main

func findDisappearedNumbers(nums []int) []int {
	var rs []int
	s := make(map[int]struct{})

	m := len(nums)

	for _, v := range nums {
		s[v] = struct{}{}
	}
	for i := 1; i <= m; i++ {
		if _, ok := s[i]; !ok {
			rs = append(rs, i)
		}
	}

	return rs
}
