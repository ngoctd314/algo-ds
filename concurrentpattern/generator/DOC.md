# Generators

Generators are functions that return the next value in a sequence each time the function is called. This pattern can be used to implement iterators and introduce parallelism into loops. Generators in Go are implemented with goroutines.

## Parallelism

If the generator's task is computationally expensive
