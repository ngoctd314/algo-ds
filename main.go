package main

import (
	"algo/designpattern/behavioral/templatemethod/otp"
)

func main() {
	otp.Run()
}
