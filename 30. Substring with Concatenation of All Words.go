package main

import (
	"fmt"
	"strings"
)

func solve30(s string, words []string) []int {
	r := []int{}

	l := len(words[0])
	ll := len(words)
	set := make(map[string]int)

	for _, v := range words {
		set[v] = 0
	}

	ls := len(s)
	for k := range s {
		if k+ll*l <= ls {
			if _, ok := set[s[k:k+l]]; ok {
				if anagram(s[k:k+ll*l], words) {
					r = append(r, k)
				}
			}
		}
	}
	fmt.Println(8+ll*l, len(s))

	fmt.Println("r", r)

	return r
}

func anagram(s string, words []string) bool {
	w := strings.Join(words, "")

	return genKey(s) == genKey(w)
}

func genKey(s string) string {
	var key [26]int
	for _, v := range s {
		key[v-'a']++
	}

	var rs string
	for _, v := range key {
		rs += "#" + fmt.Sprint(v)
	}

	return rs
}
