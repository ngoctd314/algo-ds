package main

func solve268(n []int) int {
	l := len(n)
	s := make(map[int]interface{})
	for _, v := range n {
		s[v] = nil
	}
	for i := 0; i <= l; i++ {
		if _, ok := s[i]; !ok {
			return i
		}
	}

	return 0
}
