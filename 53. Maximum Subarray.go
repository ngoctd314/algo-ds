package main

func solve53(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}

	maxSum := nums[0]
	sum := 0
	len := len(nums)
	for i := 0; i < len; i++ {
		sum += nums[i]
		if sum > maxSum {
			maxSum = sum
		}
		if sum < 0 {
			sum = 0
		}
	}
	return maxSum
}
