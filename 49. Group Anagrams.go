package main

import (
	"fmt"
)

func solve49(in []string) [][]string {
	var anagramSet = make(map[string][]string)

	genKey := func(str string) string {
		keyArr := [26]int{}
		for _, v := range str {
			k := v - 'a'
			keyArr[int(k)]++
		}
		result := ""
		for _, v := range keyArr {
			// result += string(v)
			result += "#" + fmt.Sprint(v)
		}
		return result
	}

	for _, v := range in {
		k := genKey(v)
		anagramSet[k] = append(anagramSet[k], v)
	}

	var result = [][]string{}

	for _, v := range anagramSet {
		result = append(result, v)
	}

	return result
}
