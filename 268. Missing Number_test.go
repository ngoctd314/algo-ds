package main

import "testing"

func Test_solve268(t *testing.T) {
	type args struct {
		n []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				n: []int{3, 0, 1},
			},
			want: 2,
		},
		{
			name: "Test 2",
			args: args{
				n: []int{0, 1},
			},
			want: 2,
		},
		{
			name: "Test 3",
			args: args{
				n: []int{9, 6, 4, 2, 3, 5, 7, 0, 1},
			},
			want: 8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve268(tt.args.n); got != tt.want {
				t.Errorf("solve268() = %v, want %v", got, tt.want)
			}
		})
	}
}
