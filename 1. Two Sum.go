package main

func solve1(nums []int, target int) []int {
	var set = make(map[int]int)
	for k, v := range nums {
		if i, ok := set[target-v]; ok {
			return []int{i, k}
		}
		set[v] = k
	}
	return []int{-1, -1}
}
