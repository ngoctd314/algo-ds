package defaultinstance

import (
	"log"
	"os"
)

// GlobalLog instance
var (
	GlobalLog Logger
)

// DefaultLog is std log
type DefaultLog struct {
	warningLog *log.Logger
	infoLog    *log.Logger
	errorLog   *log.Logger
}

func init() {
	GlobalLog = DefaultLog{
		warningLog: log.New(os.Stdout, "WARN: ", log.Ldate|log.Ltime|log.Lshortfile),
		infoLog:    log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
		errorLog:   log.New(os.Stdout, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
	}
}

// SetGlobalLog set new GlobalLogger strategy
func SetGlobalLog(l Logger) {
	if l != nil {
		GlobalLog = l
	}
}

// Error level
func (d DefaultLog) Error(err interface{}) {
	d.errorLog.Println(err)
}

// Errorf level
func (d DefaultLog) Errorf(format string, params ...interface{}) {
	d.errorLog.Printf(format+"\n", params...)
}

// Warn level
func (d DefaultLog) Warn(err interface{}) {
	d.warningLog.Println(err)
}

// Warnf level
func (d DefaultLog) Warnf(format string, params ...interface{}) {
	d.warningLog.Printf(format+"\n", params...)
}

// Info level
func (d DefaultLog) Info(err interface{}) {
	d.infoLog.Println(err)
}

// Infof level
func (d DefaultLog) Infof(format string, params ...interface{}) {
	d.infoLog.Printf(format+"\n", params...)
}
