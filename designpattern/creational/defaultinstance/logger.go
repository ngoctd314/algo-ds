package defaultinstance

// Logger defines all log method
type Logger interface {
	// Error level
	Error(err interface{})
	// Errorf level
	Errorf(format string, params ...interface{})
	// Warn level
	Warn(err interface{})
	// Warnf level
	Warnf(format string, params ...interface{})
	// Info level
	Info(err interface{})
	// Infof level
	Infof(format string, params ...interface{})
}
