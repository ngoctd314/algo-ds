# Abstract Factory concept

Abstract Factory is a creational design pattern, which solves the problem of creating entire product families without specifying their concrete classes.

Abstract Factory defines an interface for creating all distinct products but leaves the actual product creation to concrete factory classes. Each factory type corresponds to a certain product variety.

The client code calls the creation methods of a factory object instead of creating products directly with a constructor call. Since a factory corresponds to a single product variant, all its products will be compatible.

## Problem

You have many product with different brand (same product type but differrent brand). You need a way to create objects so that they much other object of the same brand. Customers get quite mad when they receive non-matching brand.
Also you don't want to change existing code when adding new products or families of products to the program. Vendors update their catalogs very often, and you wouldn't want to change the core code each time it happens.

## Solution

The first thing the Abstract Factory patterns suggests is to explicitly declare interfaces for each distinct product of the product family. Then you can make all variants of products follow those interfaces.

## Applicability

- Use the Abstract Factory when your code needs to work with various families of related products, but you don't want it to depend on concrete classes of those products - they might be unknow beforehand or you simply want to allow for future extensibility.

- The Abstract Factory provides you with an interface for creating objects from each class of the product family. As long as your code creates objects via this interface, you don't have to worry about creating the wrong variant of a product which doesn't match the products already created by your app.
