package house

type house struct {
	windowType string
	doorType   string
	floor      int
}
