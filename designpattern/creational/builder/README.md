# Builder

## Intent

Builder is a creational design pattern that let you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same contruction code.

In most cases most of the parameters will be unused, making the constructor calls pretty ugly. For instance, only a fraction of houses have swimming pools, so the parameters related to swimming pools will be useless nine times out of ten.

## Solution

The Builder pattern suggests that you extract the object construction code out of its own class and move it to separate objects called builders.

The pattern organizes object construction into a set of steps. To create an object, you execute a series of these steps on a builder object. The important part is that you don't need to call all of the steps. You can call only those steps that are necessary for producing a particular configuration of an object.

## Director

You can go futher and extract a series of calls to the builder steps you use to construct a product into a separate class called director. The director defines order in which to execute the building steps, while the builder provides the implementation for those steps.

Having a director class in your program isn't strictly necessary. You can always call the building steps in a specific order directly from the client code. However, the director class might be a good place to put various construction routines so you can reuse them across your program.

In addition, the director class completely hides the details of product construction from the client code. The client only needs to associate a builder with a director, lauch the construction with the director, and get the result from the builder.

## Applicability

### Use the Builder pattern to get rid of a "telescopic constructor"

Say you have a constructor with ten optional parameters. Calling such a beast is very inconvenient; therefore, you overload the constructor and create several shorter versions with fewer parameters. These constructors still refer to the main one, passing some default values into any omitted parameters.

```java
class Pizza {
    Pizza(int size) {...}
    Pizza(int size, boolean cheese) {...}
    Pizza(int size, boolean cheese, boolean pepperoni) {...}
}
```

The Builder pattern lets you build objects step by step, using only those steps that you really need. After implementing the pattern, you don't have to cram dozens of parameters into your constructors anymore.

### Use the Builder pattern when you want your code to be able to create different representations of some product

The Builder pattern can be applied when construction of various representations of the product involves similar steps that differ only in the details.

The base builder interface define all possible construction steps, and concrete builders implement these steps to construct particular representations of the product. Meanwhile, the director class guides the order of construction

### Use the builder to construct Composite trees or other complex object

The builder pattern lets you construct products step-by-step. You could defer execution of some steps without breaking the final product.

The builder doesn't expose the unfinished product while running construction steps. This prevents the client code from fetching an incomplete result.

## How to Implement

- Make sure that you can clearly the common construction steps for building all avaiable product representations. Otherwise, you won't be able to proceed with implementing the pattern.
- Declare these steps in the base builder interface
- Create a concrete builder class for each of the product representations and implement their construction steps.

Don't forget about implementing a method for fetching the result of the construction. The reason why this method can't be declared inside the builder interface is that various builders may construct products that don't have a common interface.

- Think about creating a director class. It may encapsulate various ways to construct a product using the same builder object.

## Pros and Cons

### Pros

- You can construct objects step-by-step, defer construction steps or run steps recursively.
- You can reuse the same construction code when building various representations of products.
- Single Responsibility Principle. You can isolate complex construction code from the business logic of the product.

### Cons

- The overral complexity of the code increases since the pattern requires creating multiple new classes.

## Conceptual Example

The Builder pattern is used when the desired product is complex and requires multiple steps to complete. In this case, several construction methods would be simpler than a single monstrous constructor.
