package rtb

type rtbDirector struct {
	builder rtbBuilder
}

func newRTBDirector(r rtbBuilder) *rtbDirector {
	return &rtbDirector{
		builder: r,
	}
}

func (d *rtbDirector) buildPage() ([]byte, error) {
	var (
		err  error
		step = d.builder
	)

	err = d.chain(
		step.buildRequestContext,
		step.buildPmpDealMap,
		step.buildBidRequest,
		step.buildPmpPartnerSet,
	)
	if err != nil {
		return nil, err
	}

	result, err := d.builder.GetResult()
	if err != nil {
		return nil, err
	}

	return result, nil

}

type buildStep func() error

func (d *rtbDirector) chain(steps ...buildStep) error {
	var err error
	for _, step := range steps {
		err = step()
		if err != nil {
			return err
		}
	}
	return nil
}
