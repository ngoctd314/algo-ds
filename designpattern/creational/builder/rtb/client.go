package rtb

import "log"

func Run() {
	page := newPage()

	pageDirector := newRTBDirector(page)
	result, err := pageDirector.buildPage()
	if err != nil {
		log.Println("error occurs when build page: ", err)
	}

	log.Println("build success: ", string(result))
}
