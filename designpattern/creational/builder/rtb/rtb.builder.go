package rtb

// 1. Define builder interface
// rtbBuilder contain all step to build complete realtime bidding process for video
type rtbBuilder interface {
	// build_RequestContext method construct request context for realtime bidding process
	buildRequestContext() error
	// build_PmpDeal method construct Private Marketplace deal
	buildPmpDealMap() error
	// build_BidRequest method construct BidRequest object from RequestContext object
	buildBidRequest() error
	// build_PmpPartnerSet method construct Partnet Set
	buildPmpPartnerSet() error
	// build_DSPCompanySet method construct DSP company
	buildDSPCompanySet() error
	// build_CompanyBidRequest method construct a map with key: Company, value BidRequest
	buildCompanyBidRequestMap() error
	// build_DSPResponse method send bid request to DSP and
	buildDSPResponse() error
	buildDSPDeal() error
	buildLogDeal() error
	buildHideLabel() error
	buildWinner() error
	buildVideoLog() error

	GetResult() ([]byte, error)
}
