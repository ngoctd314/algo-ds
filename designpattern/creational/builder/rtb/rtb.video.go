package rtb

type page struct {
	plugins interface{}

	reqCtx               string
	pmpDeal              string
	bidReq               string
	pmpPartnetSet        string
	dspCompanySet        string
	mapCompanyBidRequest string
}

func newPage() *page {
	return new(page)
}

// build_RequestContext method construct request context for realtime bidding process
func (p *page) buildRequestContext() error {
	p.reqCtx = "page request context"
	return nil
}

// build_PmpDeal method construct Private Marketplace deal
func (p *page) buildPmpDealMap() error {
	p.pmpDeal = "page pmp deal"
	return nil
}

// build_BidRequest method construct BidRequest object from RequestContext object
func (p *page) buildBidRequest() error {
	p.bidReq = "page bid request"
	return nil
}

// build_PmpPartnerSet method construct Partnet Set
func (p *page) buildPmpPartnerSet() error {
	p.pmpPartnetSet = "page pmp partner set"
	return nil
}

// build_DSPCompanySet method construct DSP company
func (p *page) buildDSPCompanySet() error {
	panic("not implemented") // TODO: Implement
}

// build_CompanyBidRequest method construct a map with key: Company, value BidRequest
func (p *page) buildCompanyBidRequestMap() error {
	panic("not implemented") // TODO: Implement
}

// build_DSPResponse method send bid request to DSP and
func (p *page) buildDSPResponse() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) buildDSPDeal() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) buildLogDeal() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) buildHideLabel() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) buildWinner() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) buildVideoLog() error {
	panic("not implemented") // TODO: Implement
}

func (p *page) GetResult() ([]byte, error) {
	result := p.reqCtx + " " + p.bidReq + " " + p.pmpDeal + " " + p.pmpPartnetSet

	return []byte(result), nil
}
