# Bridge

Bridge is a structural design pattern that divides business logic or huge class into separate class hierarchies that can be developed independently.

One of these hierarchies (often called the Abstraction) will get a reference to an object of the second hierarchy  (Implementation). The abstraction will be able to delegate some of its calls to the implementations object. Since all implementations will have a common interface, they'd be interchangeable inside the abstraction.

This pattern suggests dividing a large class into two separate hierarchy 

- Abstraction - It is interface and children of the Abstraction are referred to as Refined Abstraction. The abstraction contains a reference to the implementation.

- Implementation - It is also an interface and children's of the Implementation are referred to as a Concrete Implementation. 

Abstraction hierarchy is being referred to by clients without worrying about the implementation.

## Intent

Bridge is a structural design pattern that lets you split a large class or set of closely related classes into separate hierarchies - abstraction and implementation - which can be developed independently of each other.

## Abstraction and Implementation

The GoF book introduces the terms Abstraction and Implementation as part of the Bridge definition.

Abstraction (also called interface) is a high-level control layer for some entity. This layer isn't supposed to do any real work on its own. It should delegate the work to the implementation layer (also called platform).

When talking about real applications, the abstraction can be represented by a graphical user interface (GUI), and the implementation could be the underlying os code (API) which the GUI layer calls in response to user interactions.

Generally speaking, you can extend such an app in two different directions:

- Have several different GUIs (for instance, tailored for regular customers or admins).

- Support several different APIs (for example, to be able to lauch the app under windows, linux and macOS)

In a worst-case scenario, this app might look like a giant spaghetti bowl, where hundreds of conditionals connect different types of GUI with various APIs all over the code.

Making even a simple change to a monolithic codebase is pretty hard because you must understand the entire thing very well. Making changes to smaller, well-defined modules is much easier.

You can bring order to this chaos by extracting the code related to specific interface-platform combinations into separate classes. However, soon you'll discover there are lots of these classes. The class hierarchy will grow exponentially because adding a new GUI or supporting a different API would require creating more and more classes.

Let's try to solve this problem this issue with the Bridge pattern. It suggests that we divide the classes into two hierarchies:

- Abstraction: the GUI layer of the APP
- Implementation: the operating system's APIs

The abstraction object controls the appearance of the app, delegating and actual work to linked implementation object. Different implementations are interchangeable as long as they follow a common interface, enabling the same GUI to work under Windows and Linux.

## Applicability

## Example

You have two types of computers: Mac and Windows. Also, two types of pointers: Epson and HP. Both computers and printers need to work with each other in any combination. The client doesn't want to worry about the details of connecting printers to computers.

If we introduce new printers, we don't want our code to grow exponentially. Instead of creating four structs for the 2*2 combination, we create two hierarchies:

- Abstraction hierarchy: this will be our computers
- Implementation hierarchy: this will be our printers

These two hierarchies communicate with each other via Bridge, where the Abstraction (computer) contains a reference to the implementation (printer). Both the Abstraction and implementation can be developed independently without affecting each other.
