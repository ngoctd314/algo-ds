package bridge

// abstraction
type computer interface {
	print()
	setPrinter(printer)
}
