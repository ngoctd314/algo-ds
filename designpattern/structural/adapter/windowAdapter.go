package adapter

import "fmt"

type windowsAdapter struct {
	*windows
}

func (w *windowsAdapter) insertIntoLightningPort() {
	fmt.Println("Adater converts lightning signal to USB")
	w.insertIntoUSBPort()
}
