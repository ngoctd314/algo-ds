# Adapter

Adapter is a structural design pattern that allows objects will incompatible interfaces to collaborate.

The Adapter acts as a wrapper object between two objects. It catches calls for one object and transforms them to format and interface recognizable by the second object.

## Applicability

- Use the Adapter class when you want to use some existing class, but its interface isn't compatible with the rest of your code.
The Adapter pattern lets you create a middle-layer class that serves as a traslator between your code and a legacy class, a 3rd-party class or any other class with a weird interface.

- Use the pattern when you want to reuse several existing subclasses that lack some common funtionality that can't be added to the superclass.

## Conceptual Example

We have a client code that expects some features of an object (Lightning port), but we have another object called adaptee (Windows Laptop) which offers the same functionality but through a different interface (USB port).

