# Facade

Facade is a structural design pattern that provides a simplied interface to a library, a framework, or any other complex set of classes.

While Facade decreases the overrall complexity of the application, it also helps to move unwanted dependencies to one place.

## Conceptual Example

It's easy to underestimate the complexities that happen behind the scenes when you order a pizza using your credit card. There are dozens of subsystems that are acting this process.

- Check account
- Check security PIN
- Credit/debit balance
- Make ledger entry
- Send notification
