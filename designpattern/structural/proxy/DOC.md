# Proxy

Proxy is a structural design pattern that lets you provide a substitute or placeholder for another object. A proxy controls access to the original object, allowing you to perform something either before or after the request gets through to the original object.

A proxy can receives client requests, does some work (access control, caching, ect). and then passes the request to a service object.
The proxy object has the same interface as a service, which makes it interchangeable with a real object when passed to a client.

## Example

A web server such as Nginx can act as a proxy for your application server.

- It can do rate limiting.
- It can do rate limiting.
- It can do request caching.
