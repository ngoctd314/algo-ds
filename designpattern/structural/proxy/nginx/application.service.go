package nginx

type application struct{}

func newAppService() *application {
	return new(application)
}

func (s *application) handleRequest(url, method string) (int, string) {
	if url == "/app/status" && method == "GET" {
		return 200, "OK"
	}

	if url == "/create/user" && method == "POST" {
		return 201, "User Created"
	}

	return 404, "Not OK"
}
