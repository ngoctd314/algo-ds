package nginx

type nginx struct {
	app               *application
	maxAllowedRequest int
	rateLimiter       map[string]int
}

func newNginxProxy(app *application) *nginx {
	return &nginx{
		app:               app,
		maxAllowedRequest: 2,
		rateLimiter:       map[string]int{},
	}
}

func (s *nginx) handleRequest(url, method string) (int, string) {
	if allowed := s.checkRateLimiting(url); !allowed {
		return 403, "Not Allowed"
	}

	return s.app.handleRequest(url, method)
}

func (s *nginx) checkRateLimiting(url string) bool {
	s.rateLimiter[url]++
	if s.rateLimiter[url] > s.maxAllowedRequest {
		return false
	}

	return true
}
