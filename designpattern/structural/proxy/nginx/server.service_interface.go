package nginx

type server interface {
	handleRequest(string, string) (int, string)
}

// declare all implemnt here
var (
	// application implement server
	_ server = new(application)
)
