package decorator

type cheeseTopping struct {
	pizza pizza
}

func (p *cheeseTopping) getPrice() int {
	pizzaPrice := p.pizza.getPrice()
	return pizzaPrice + 10
}

func (p *cheeseTopping) getTopping() []string {
	return append(p.pizza.getTopping(), "cheese")
}

func withCheeseTopping(p pizza) pizza {
	return &cheeseTopping{
		pizza: p,
	}
}
