package decorator

import "fmt"

type basePizza struct {
	price   int
	topping []string
}

func (p *basePizza) getPrice() int {
	return p.price
}

func (p *basePizza) getTopping() []string {
	return []string{}
}

func (p *basePizza) print() {
	fmt.Printf("Price: %v\nTopping: %v\n", p.getPrice(), p.getTopping())
}

func addTopping(pizza pizza, withToppings ...withTopping) pizza {
	for _, withTopping := range withToppings {
		pizza = withTopping(pizza)
	}

	return pizza
}

func Run() {
	var basePizza pizza = &basePizza{
		price:   10,
		topping: []string{},
	}
	basePizza = addTopping(basePizza, withCheeseTopping, withTomatoTopping, withVeggeManiaTopping)
	switch basePizza.(type) {
	case *veggeMania:
		fmt.Println("veggeMania")
	}

	fmt.Printf("Price: %v\nTopping: %v\n", basePizza.getPrice(), basePizza.getTopping())
}
