package decorator

type pizza interface {
	getPrice() int
	getTopping() []string
}

type withTopping func(pizza) pizza
