package decorator

type tomatoTopping struct {
	pizza pizza
}

func (p *tomatoTopping) getPrice() int {
	pizzaPrice := p.pizza.getPrice()
	return pizzaPrice + 7
}

func (p *tomatoTopping) getTopping() []string {
	return append(p.pizza.getTopping(), "tomato")
}

func withTomatoTopping(p pizza) pizza {
	return &tomatoTopping{
		pizza: p,
	}
}
