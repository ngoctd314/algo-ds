# Observer

Observer is a behavioral design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happens to the object they're observing.

## Problem

You have two types of objects: a Customer and a Store. The customer is very interested in a particular brand of product which should become avaiable in the store very soon.

The customer could visit the store every day and check product avaiability. But while the product is still en route, most of these trips would be pointless.

On the other hand, the store could send tons of emails (which might be considered spam) to all customers each time a new product becomes available. This would save some customers from endless trips to the store. At the same time, it'd upset other customers who aren't interested in new products.

It looks like we're got a conflict. Either the customer wastes time checking product availability or the store wastes resources notifying the wrong customers.

## Solution

The object that has some interesting state is often called subject, but since it's also going to notify other objects about the changes to its state, we'll call it publisher. All other objects that want to track changes to the publisher' state called subscribers.

The Observer pattern suggests that you add a subscription mechanism to the publisher class so individual objects can subscribe to or unsubscribe from a stream of events coming from that publisher.

Whenever an important event happens to the publisher, it goes its subscribers and calls the specific notification method on their objects.

If your app has several different types of publishers and you want to make your subscribers compatible with all of them, you can go even futher and make all publishers follow the same interface. This interface would only need to describe a few subscription methods. The interface would allow subscribers to observe publisher's states without coupling to their concrete classes.

Publisher notifies subscribers by calling the specific notification method on their objects.

## Structure

## Applicability

- Use the Observer pattern when changes to the state of one object may require changing other objects and the actual set of objects is unknown beforehand or changes dynamically.

- Use the pattern when some objects in your app must observe others, but only for a limited time or in specific cases.
The subscription list is dynamic, so subscribers can join or leave the list whenever they need to.

## How to implement

## Pros and Cons

\+ Open/Closed Principle. You can introduce new subsriber classes without having to change the publisher's code.

\+ You can establish relations between objects at runtime.

\- Subscribers are notified in random order.
