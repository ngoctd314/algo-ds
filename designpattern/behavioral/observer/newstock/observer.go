package newstock

type observer interface {
	update(string)
	getID() string
}
