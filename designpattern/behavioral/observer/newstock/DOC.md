# Observer

Observer is a behavioral design pattern that allows some objects to noftify other objects about changes in their state.
The Observer pattern provides a way to subscribe and unsubscribe to and from these events for any object that implements a subsriber interface.

## Conceptual Example

In the e-commerce website, items go out stock from time to time. There can be customers who are interested in a particular item that went out of stock. There are three solutions to this problem:

1. The customer keeps checking the availability of the item at some frequency.

2. E-commerce bombards customers with all new items avaiable, which are in stock.

3. The customer subscribes only to the particular item he is interested in and gets notified if the item is avaiable. Also, multiple customers can subscribe to the same product.

The major components of the observer pattern are:

- Subject, the instance which publishes an event when anything happens.
- Observer, which subscribes to the subject events and gets notified when they happen.