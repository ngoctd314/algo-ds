package strategy

func Run() {
	lfu := &lfu{}

	cache := initCache(lfu)
	cache.add("name", "TDN")
	cache.add("age", "22")

	cache.add("country", "VN")

	fifo := &fifo{}
	cache.setEvictionAlgo(fifo)

	cache.add("phone", "0339229174")

	cache.setEvictionAlgo(lfu)
	cache.add("gender", "male")

}
