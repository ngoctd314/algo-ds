package otp

import (
	"fmt"
	"math/rand"
	"strconv"
)

// concrete implementation
type sms struct {
}

func (s *sms) genRandomOTP(len int) string {
	randomOTP := rand.Intn(len)
	fmt.Printf("SMS: generating random otp %d\n", randomOTP)

	return strconv.Itoa(randomOTP)
}

func (s *sms) saveOTPCache(otp string) {
	fmt.Printf("SMS: saving otp: %s to cache\n", otp)
}

func (s *sms) getMessage(otp string) string {
	return "SMS OTP for login is " + otp
}

func (s *sms) sendNotification(message string) error {
	fmt.Printf("SMS: sending sms: %s\n", message)
	return nil
}

func (s *sms) publishMetric() {
	fmt.Printf("SMS: publishing metrics\n")
}
