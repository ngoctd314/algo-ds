# Template Method

Template Method is a behavioral design pattern that defineds the skeleton of an algorithm in the superclass but lets subclasses override specific step of the algorithm without changing its structure.

## Applicability

- Use the Template Method pattern when you want to let clients extend only particular steps of an algorithm, but not the whole algorithm or its structure.

The Template Method lets you turn a monolithic algorithm into a series of individual steps which can be easily extended by subclasses while keeping intact the structure defined in a superclass.

- Use the pattern when you have several classes that contain almost identical algorithms with some minor differences. As a result, you might need to modify all classes when the algorithm changes.

When you turn such an algorithm into a template method, you can also pull up the steps with similar implementations into superclass, eliminating code duplication. Code that varies between subclasses can remain in subclasses.

## Concept

Let's consider the example of One Time Password (OTP) functionality. There are different ways that the OTP can be delivered to a user (SMS, email, etc). But irrespective whether it's an SMS or email OTP, the entre OTP process is the same:

1. Generate a random n digit number
2. Save this number in the cache for later verification
3. Prepare the content
4. Send the notification
5. Publish the metric

Any new OTP types that will be introduced in the future most likely still go through the above steps

So, we have a scenario where the steps of the particular operation are the same, but these step's implement may differ. This is an appropriate situation to consider using the Template Method pattern.
