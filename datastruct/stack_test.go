package datastruct

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_newStack(t *testing.T) {
	t.Run("new stack", func(t *testing.T) {
		st := newStack()
		require.Nil(t, st.top)
		require.Equal(t, 0, st.length)
	})
}

func Test_stack_len(t *testing.T) {
	type fields struct {
		top    *stNode
		length int
	}

	st := newStack()
	st.push(1)

	t.Run("stack len", func(t *testing.T) {
		require.Equal(t, 1, st.len())
		st.push(2)
		require.Equal(t, 2, st.len())

	})
}

func Test_stack_peek(t *testing.T) {
	st := newStack()

	require.Nil(t, st.peek())

	st.push(0)
	require.Equal(t, 0, st.peek())
	require.Equal(t, 1, st.len())

	st.push(1)
	require.Equal(t, 1, st.peek())
	require.Equal(t, 2, st.len())
}

func Test_stack_pop(t *testing.T) {
	st := newStack()

	require.Nil(t, st.pop())

	st.push(0)
	require.Equal(t, 1, st.len())
	require.Equal(t, 0, st.pop())
	require.Equal(t, 0, st.len())
}

func Test_stack_push(t *testing.T) {
	st := newStack()

	st.push(0)
	require.Equal(t, 0, st.peek())
	require.Equal(t, 1, st.len())
}
