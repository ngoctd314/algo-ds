package datastruct

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewSet(t *testing.T) {
	newSet := NewSet()

	require.NotNil(t, newSet)
	require.Empty(t, newSet)
}

func TestSet_Add(t *testing.T) {
	set := NewSet()

	set.Add("1")
	require.NotEmpty(t, set)
	require.Equal(t, 1, set.Len())

	// duplicate set
	set.Add("1")
	require.Equal(t, 1, set.Len())

	// key with type int
	set.Add(1)
	require.Equal(t, 2, set.Len())
}

func TestSet_Delete(t *testing.T) {
	set := NewSet()

	set.Add(1)

	// key with type string (not int)
	set.Delete("1")
	require.Equal(t, set.Len(), 1)

	set.Delete(1)
	require.Equal(t, set.Len(), 0)
}

func TestSet_Exist(t *testing.T) {
	set := NewSet()

	set.Add(1)
	// key with type string (not int)
	require.False(t, set.Exist("1"))
	require.True(t, set.Exist(1))

}
