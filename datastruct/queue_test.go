package datastruct

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_newQueue(t *testing.T) {
	q := newQueue()

	require.Nil(t, q.head)
	require.Nil(t, q.tail)
	require.Zero(t, q.length)
}

func Test_push(t *testing.T) {
	q := newQueue()

	t.Run("test push to empty queue", func(t *testing.T) {
		q.push(1)
		require.NotNil(t, q.head)
		require.NotNil(t, q.tail)
		require.Equal(t, q.length, 1)
	})

	t.Run("test push to not empty queue", func(t *testing.T) {
		q.push(2)
		require.Equal(t, q.length, 2)
	})
}

func Test_pop(t *testing.T) {
	q := newQueue()

	t.Run("test pop empty queue", func(t *testing.T) {
		rs := q.pop()

		require.Nil(t, rs)
	})

	t.Run("test pop queue with 1 item", func(t *testing.T) {
		q.push(1)
		rs := q.pop()

		// expect not nil result
		require.NotNil(t, rs)
		// expect exact result value
		require.Equal(t, rs, 1)
		// expect empty queue
		require.Nil(t, q.head)
		require.Nil(t, q.tail)
		require.Zero(t, q.length)
	})

	t.Run("test pop queue with > 1 item", func(t *testing.T) {
		q.push(1)
		q.push(2)

		rs := q.pop()

		// expect not nil  result
		require.NotNil(t, rs)
		// expect exact result
		require.Equal(t, rs, 1)
		// expect tail === head
		require.Equal(t, q.tail, q.head)
	})

}

func Test_peek(t *testing.T) {
	q := newQueue()

	t.Run("test peek empty queue", func(t *testing.T) {
		require.Nil(t, q.peek())
	})

	t.Run("test peek queue with 1 item", func(t *testing.T) {
		q.push(1)
		rs := q.peek()

		require.NotNil(t, rs)
		require.Equal(t, rs, 1)
		require.Equal(t, q.length, 1)

		require.NotNil(t, q.head)
		require.NotNil(t, q.tail)
		require.Equal(t, q.head, q.tail)
	})
}
