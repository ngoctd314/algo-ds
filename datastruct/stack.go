package datastruct

type (
	stack struct {
		top    *stNode
		length int
	}
	stNode struct {
		value interface{}
		prev  *stNode
	}
)

// newStack create nil stack with length = 0
func newStack() *stack {
	return &stack{
		top:    nil,
		length: 0,
	}
}

// len return the number of items in the stack
func (s *stack) len() int {
	return s.length
}

// peek view the top item on the stack
func (s *stack) peek() interface{} {
	if s.length == 0 {
		return nil
	}
	return s.top.value
}

func (s *stack) pop() interface{} {

	if s.length == 0 {
		return nil
	}
	s.length--
	result := s.top.value

	s.top = s.top.prev

	return result

}

func (s *stack) push(value interface{}) {
	s.length++

	newStNode := &stNode{
		value: value,
		prev:  s.top,
	}
	if s.length > 0 {
		newStNode.prev = s.top
	}

	s.top = newStNode
}
