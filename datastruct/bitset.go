package datastruct

// Bitset ..
type Bitset interface {
	GetBit(i int) bool
	SetBit(i int, value bool)
	Len() int
}

// BitSetBool .
type BitSetBool []bool

// GetBit .
func (b BitSetBool) GetBit(i int) bool {
	return b[i]
}

// SetBit .
func (b BitSetBool) SetBit(i int, value bool) {
	b[i] = value
}

// Len .
func (b BitSetBool) Len() int {
	return len(b)
}
