package datastruct

// Set datastruct
type Set map[interface{}]struct{}

// NewSet create empty set
func NewSet() Set {
	return make(Set)
}

// Add one or more key to set
func (s Set) Add(keys ...interface{}) {
	for _, k := range keys {
		s[k] = struct{}{}
	}
}

// Delete remove one or more key from set
func (s Set) Delete(keys ...interface{}) {
	for _, k := range keys {
		if s.Exist(k) {
			delete(s, k)
		}
	}
}

// Exist check it key is in set
func (s Set) Exist(keys interface{}) bool {
	_, ok := s[keys]
	return ok
}

// Len return length of set
func (s Set) Len() int {
	return len(s)
}
