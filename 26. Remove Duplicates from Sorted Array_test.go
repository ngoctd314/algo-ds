package main

import (
	"reflect"
	"testing"
)

func Test_solve26(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				nums: []int{1, 1, 2, 3},
			},
			want: []int{1, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve26(tt.args.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solve26() = %v, want %v", got, tt.want)
			}
		})
	}
}
