package main

import "fmt"

func nextGreaterElement(nums1 []int, nums2 []int) []int {
	var rs []int

	s := make(map[int]int)

	l := len(nums2)
	// last element => -1
	s[nums2[l-1]] = -1
	for i := 0; i < l-1; i++ {
		j := nums2[i]
		k := nums2[i+1]
		s[j] = -1
		if j < k {
			s[j] = k
		}
	}
	fmt.Println(s)

	for _, v := range nums1 {
		rs = append(rs, s[v])
	}

	return rs
}
