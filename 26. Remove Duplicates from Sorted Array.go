package main

import "fmt"

func fn(nums []int) int {
	fmt.Println("RUN")
	return len(nums)
}

func solve26(nums []int) int {
	l := 0
	for i := 0; i < fn(nums); i++ {
		if i == 0 || nums[i] != nums[i-1] {
			nums[l] = nums[i]
			l++
		}
	}
	fmt.Println(nums)

	return l
}
