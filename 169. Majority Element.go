package main

type set map[int]int

func solve169(nums []int) int {
	newSet := make(set)

	len := len(nums)/2 - 1

	for _, v := range nums {
		if _, ok := newSet[v]; ok {
			if newSet[v] > len {
				return v
			}
		}
		newSet[v]++
	}
	return nums[0]
}
