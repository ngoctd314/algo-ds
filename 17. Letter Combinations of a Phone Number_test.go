package main

import (
	"reflect"
	"testing"
)

func Test_solve17(t *testing.T) {
	type args struct {
		digits string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "",
			args: args{
				digits: "22",
			},
			want: []string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve17(tt.args.digits); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solve17() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_concatarr(t *testing.T) {
	type args struct {
		a1 []string
		a2 []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "",
			args: args{
				a1: []string{"a", "b"},
				a2: []string{"e", "f"},
			},
			want: []string{"ae", "af", "be", "bf"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := concatarr(tt.args.a1, tt.args.a2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("concatarr() = %v, want %v", got, tt.want)
			}
		})
	}
}
