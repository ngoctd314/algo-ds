package main

import "testing"

func Test_solve242(t *testing.T) {
	type args struct {
		s string
		t string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test 1",
			args: args{
				s: "anagram",
				t: "nagaram",
			},
			want: true,
		},
		{
			name: "Test 2",
			args: args{
				s: "rat",
				t: "car",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve242(tt.args.s, tt.args.t); got != tt.want {
				t.Errorf("solve242() = %v, want %v", got, tt.want)
			}
		})
	}
}
