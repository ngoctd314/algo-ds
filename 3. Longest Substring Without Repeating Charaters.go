package main

import (
	"strings"
)

func solve3(s string) int {
	if len(s) == 0 || len(s) == 1 {
		return len(s)
	}
	m := 0
	arr := strings.Split(s, "")
	l := len(s)
	var set map[string]struct{}

	// i < fn()
	for i := 0; i < l-1; i++ {
		set = make(map[string]struct{})
		set[arr[i]] = struct{}{}
		for j := i + 1; j < l; j++ {
			if _, ok := set[arr[j]]; ok {
				break
			}
			set[arr[j]] = struct{}{}
		}
		if len(set) >= m {
			m = len(set)
		}
	}

	return m
}

func solve3n(s string) int {
	m := 0
	start := 0
	l := len(s)
	arr := strings.Split(s, "")

	var set map[string]struct{}
	set = make(map[string]struct{})

	for i := 0; i < l; {
		if _, ok := set[arr[i]]; ok {
			if len(set) >= m {
				m = len(set)
			}
			set = make(map[string]struct{})
			start++
			i = start
		} else {
			set[arr[i]] = struct{}{}
			if i == l-1 && len(set) >= m {
				m = len(set)
			}
			i++
		}
	}

	return m
}
