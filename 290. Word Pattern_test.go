package main

import "testing"

func Test_solve290(t *testing.T) {
	type args struct {
		p string
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Test 1",
			args: args{
				p: "abba",
				s: "dog cat cat dog",
			},
			want: true,
		},
		{
			name: "Test 2",
			args: args{
				p: "abba",
				s: "dog cat cat fish",
			},
			want: false,
		},
		{
			name: "Test 3",
			args: args{
				p: "aaaa",
				s: "dog cat cat dog",
			},
			want: false,
		},
		{
			name: "Test 4",
			args: args{
				p: "abba",
				s: "dog dog dog dog",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve290(tt.args.p, tt.args.s); got != tt.want {
				t.Errorf("solve290() = %v, want %v", got, tt.want)
			}
		})
	}
}
