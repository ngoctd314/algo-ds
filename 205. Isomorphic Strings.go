package main

import (
	"fmt"
)

func isIsomorphic(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}
	l := len(s)

	s1 := make(map[byte]string)
	s2 := make(map[byte]string)

	for k := range s {
		s1[s[k]] += fmt.Sprint(k)
		s2[t[k]] += fmt.Sprint(k)
	}

	for i := 0; i < l; i++ {
		if s1[s[i]] != s2[t[i]] {
			return false
		}
	}

	return true
}
