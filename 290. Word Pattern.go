package main

import "strings"

func solve290(p string, s string) bool {
	m := make(map[byte]string)

	sa := strings.Split(s, " ")
	l := len(sa)
	for i := 0; i < l; i++ {
		if v, ok := m[p[i]]; ok {
			if v != sa[i] {
				return false
			}
		} else {
			m[p[i]] = sa[i]
		}
	}

	return true
}
