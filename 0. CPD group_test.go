package main

import (
	"reflect"
	"testing"
)

func Test_genDspGroup(t *testing.T) {
	type args struct {
		dspIDZoneIDList map[int][]int
		dspIDList       []int
	}
	tests := []struct {
		name string
		args args
		want [][]int
	}{
		{
			name: "",
			args: args{
				dspIDZoneIDList: map[int][]int{
					1: {1},
					2: {2, 3},
					3: {1, 2},
				},
				dspIDList: []int{1, 2, 3},
			},
			want: [][]int{{1, 2, 3}, {2, 3, 1}, {1, 2}},
		},
		{
			name: "",
			args: args{
				dspIDZoneIDList: map[int][]int{
					1: {1, 2, 4},
					2: {1, 3},
					3: {3},
					4: {1, 3, 4},
					5: {2, 4},
				},
				dspIDList: []int{1, 2, 3, 4, 5},
			},
			want: [][]int{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := genDspGroup(tt.args.dspIDZoneIDList, tt.args.dspIDList); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("genDspGroup() = %v, want %v", got, tt.want)
			}
		})
	}
}
