package main

import (
	"strings"
)

func solve17(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	rs := []string{}

	set := map[string][]string{
		"2": {"a", "b", "c"},
		"3": {"d", "e", "f"},
		"4": {"g", "h", "i"},
		"5": {"j", "k", "l"},
		"6": {"m", "n", "o"},
		"7": {"p", "q", "r", "s"},
		"8": {"t", "u", "v"},
		"9": {"w", "x", "y", "z"},
	}
	if len(digits) == 1 {
		return set[digits]
	}

	arr := strings.Split(digits, "")
	r := set[arr[0]]
	for _, v := range arr[1:] {
		r = concatarr(r, set[v])

	}

	return rs
}

func concatarr(a1 []string, a2 []string) []string {
	rs := []string{}
	for k1 := range a1 {
		for _, v2 := range a2 {
			rs = append(rs, a1[k1]+v2)
		}
	}

	return rs
}
