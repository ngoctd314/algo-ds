package main

import "testing"

func Test_solve387(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				s: "leetcode",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve387(tt.args.s); got != tt.want {
				t.Errorf("solve387() = %v, want %v", got, tt.want)
			}
		})
	}
}
