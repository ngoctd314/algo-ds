package main

import (
	"strconv"
)

func solve242(s string, t string) bool {
	fn := func(str string) string {
		rs := make([]int, 26)
		// loop 26 character
		for i := 0; i < len(str); i++ {
			j := str[i] - 'a'
			rs[int(j)]++
		}

		e := ""
		for _, v := range rs {
			e += strconv.Itoa(v)
		}
		return e
	}
	if fn(s) == fn(t) {
		return true
	}

	return false
}
