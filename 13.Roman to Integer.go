package main

func solve13(in string) int {
	var s = make(map[string]int)
	s["I"] = 1
	s["V"] = 5
	s["X"] = 10
	s["L"] = 50
	s["C"] = 100
	s["D"] = 500
	s["M"] = 1000

	var result int

	for _, v := range in {
		result += s[string(v)]
	}

	return result
}
