# Interface Segregaton Principle

Many client-specific interfaces are better than one general-purpose interface

Clients should not be forced to depend on methods they do not use.

Fat interfaces indicate more responsibility and, as we saw with the SRP, the more responsibility an object has, the more likely it will want to change.

## Advantage

- ISP requires us to define thin interfaces

- ISP leads to explicit inputs.
