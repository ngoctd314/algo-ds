package srp

import (
	"fmt"
	"math"
)

type circle struct {
	radius float32
}

type square struct {
	length float32
}

func (c circle) area() float32 {
	return c.radius * c.radius * math.Pi
}

func (c circle) result() {
	fmt.Println(c.area())
}

func (s square) area() float32 {
	return s.length * s.length
}

func (s square) result() {
	fmt.Println(s.area())
}
