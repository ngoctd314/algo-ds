# Single Responsibility Principle

There should never be more than one reason for a class to change. In other words, every class should have only one responsibility.

SRP defines that every module or class should have single responsibility and needs only one reason to change.

To much responsibility leads to coupling and makes a GOD Object (object knows everythings and does everythings).

The cause for the change is mostly due to addition of new feature, fault corrections or refactoring of code.

SRP is also closely related to the Separation of Concerns.

A class should have one, and only one, reason to change

Go doesn't have classes, but if we squint little and replace the word class with objects (structs, functions, interfaces or packages), then the concept still applies.

Each piece of code would be smaller and easier to understand, and therefore easier to test.

## Advantages

- SRP reduces the complexity by decomposing code into smaller, more consise pieces.

- SRP increases the potential reusability of code.

- SRP makes tests simpler to write and maintain.

## Example

```go
// without SRP

// Calculator calculates the test coverage for a directory
// and it's sub-directories
type Calculator struct {
	data map[string]float64
}

// Calculate will calculate the coverage
func (c *Calculator) Calculate(path string) error {
	// run
	return nil
}

// Output will print the coverage data to the supplied writer
func (c *Calculator) Output(w io.Writer) error {
	for path, result := range c.data {
		fmt.Fprintf(w, "%s -> %.1f\n", path, result)
	}

	return nil
}
```

The code looks reasonable-one member variable and two methods. Let's assume that the app was successful, and we decided that we also needed to output the results to CSV. We could add a method to do that

```go
func (c Calculator) OutputCSV(w io.Writer) {
    for path, result := range c.data {
        fmt.Fprintf(writer, "%s,%.1f\n", path, result)
    }
}
```

We have add more responsibilities to the struct and, in doing so, we have added complexity.

Let's try a different implementation:

```go
// Calculator calculates the test coverage for a directory
// and it's sub-directories
type Calculator struct {
    // coverage data
    data map[string]float64
}

// Calculate will calculate the coverage
func (c *Calculator) Calculate(path string) error {
    // run `go test -cover ./[path]/...` and store the results
    return nil
}

func (c *Calculator) getData() map[string]float64 {
    // copy and return the map
    return nil
}

type Printer interface {
    Output(data map[string]float64)
}

type DefaultPrinter struct {
    Writer io.Writer
}

// Output implements Printer
func (d *DefaultPrinter) Output(data map[string]float64) {
    for path, result := range data {
        fmt.Fprintf(d.Writer, "%s -> %.1f\n", path, result)
    }
}

type CSVPrinter struct {
    Writer io.Writer
}

// Output implements Printer
func (d *CSVPrinter) Output(data map[string]float64) {
    for path, result := range data {
        fmt.Fprintf(d.Writer, "%s -> %.1f\n", path, result)
    }
}
```

SRP is also an excellent way to improve general code readability.

```go
func loadUserHandler(resp http.ResponseWriter, req *http.Request) {
    err := req.ParseForm()
    if err != nil {
        return
    }
    userID, err := req.Form.Get("UserID")
    if err != nil {
        return
    }
    row := DB.QueryRow("SELECT * FROM Users WHERE ID = ?", userID)
    person := &Person{}
    err := row.Scan(&person.ID, &person.Name, &person.Phone)
    if err != nil {
        return
    }
    encoder := json.NewEncoder(resp)
    encoder.Encode(person)
}
```

I'd bet that took more that five seconds to understand. How about this code?

```go
func loadUserHandler(resp http.ResponseWriter, req *http.Request) {
    userID, err := extractIDFromRequest(req)
    if err != nil {
        return
    }
    person, err := loadPersonByID(userID)
    if err != nil {
        return
    }
    outputPerson(resp, person)
}
```

By applying SRP at the function level, we have reduced the function's bloat and increased its readability. The function's single responsibility is now to coordinate the calls to the other functions.
