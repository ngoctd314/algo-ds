package ocp

import (
	"fmt"
	"math"
)

type circle struct {
	radius float64
}

type square struct {
	length float64
}

type calcArea interface {
	area() float64
}

func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

func (s square) area() float64 {
	return s.length * s.length
}

func sumArea(shapes ...calcArea) float64 {
	var sum float64
	for _, shape := range shapes {
		sum += shape.area()
	}

	return sum
}

func Run() {
	c := circle{
		radius: 1,
	}
	s := square{
		length: 1,
	}

	fmt.Println(sumArea(c, s))
}
