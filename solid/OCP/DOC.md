# Open-closed Principle

The open-closed principle: Software entities should be open for extension, but closed for modification.

Open means that we should be able to extend or adapt code by adding new behaviors and features. Closes means that we should avoid making changes to existing code, changes that could result in bugs or other kinds of regression.

## Advantage

- OCP helps reduce the resk of additions and extensions

## Example

The following code does not follow the OCP:

```go
func BuildOutput(response http.ResponseWriter, format string, person Person) {
    var err error
    switch format {
    case "csv": 
        err = outputCSV(response, person)
    case "json":
        err = outputJSON(response, person)
    }
    if err != nil {
        return
    }
}
```

Just how much would have to change if we needed to add another format?

- We would need to add another case condition to the switch:
- We would need to write another formatting function
- The caller of the method would have to be updated to use the new format
- We would have to add another set of test scenarios to match the new formating

Let's replace the format input parameter and the switch statement with an abstraction, as shown in the following code:

```go
func BuildOutput(response http.ResponseWriter, format string, person Person Person) {}
```
