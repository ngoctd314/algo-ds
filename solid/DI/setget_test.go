package main

import "testing"

type mockDatabase struct {
}

func (m mockDatabase) Get(string) (string, error) {
	return "", nil
}

func (m mockDatabase) Set(string, string) error {
	return nil
}

func TestRunnerErr(t *testing.T) {
	r := newRunner(mockDatabase{})
	args := []string{}
	if err := r.run(args...); err == nil {
		t.Error("expected err on empty slice for args, got nil")
	}
}
