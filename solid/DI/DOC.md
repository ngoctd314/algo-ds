# Dependency inversion principle (DIP)

High level modules should not depend on low level modules. Both should depend on abstractions. Abstractions should not depend upon details. Details should depend on abstractions.

## Coding for User Experience

The following topics will be covered in this chapter:

- Optimizing for humans
- A security blanket named unit tests
- Test-included damage
- Visualizing your package dependencies with Godepgraph

### Export only what you must

When a method has fewer parameters, it is naturally easier to understand.

```go
NewPet("Fido", true)
```

What does true mean? It's hard to tell without opening the function of the documentation. However, what if we do the following:

```go
NewDog("Fido")
```

This this case, the purpose is clear, encapsulation is improved.

Similarly, interfaces and struct with fewer methods and packages with objects are all easier to understand, and are more likely to have a more definite purpose

```go
type WideFormatter interface {
    ToCSV(pets []Pet)([]byte, error)
    ToGOB(pets []Pet)([]byte, error)
    ToJSON(pets []Pet)([]byte, error)
}
```

Compare the preceding code to the following:

```go
type ThinFormatter interface {
    Format(pets []Pet)([]byte, error)
}

type CSVFormatter struct {}
func (f CSVFormatter) Format(pets []Pet)([]byte, error) {}

type GOBFormatter struct {}
func (f GOBFormatter) Format(pets []Pet)([]byte, error) {}

type JSONFormatter struct {}
func (f JSONFormatter) Format(pets []Pet)([]byte, error) {}
```

The result is more code. More straightforward code, but more code nontheless. Providing a better UX for users will frequently incur a little bit more cost.

## Quote

- Start with simple - get complicated only when you must
- Apply just enough abstraction
